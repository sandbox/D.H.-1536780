<?php
// $Id$

/**
 * @file
 * Content administration and module settings UI.
 */

/**
 * Menu callback; presents general easy embed media configuration options.
 */
function easy_embed_media_configure() {
  $form['thumbnail_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Thumbnail width'),
    '#default_value' => variable_get('easy_embed_media_thumbnail_width', 220),
    '#description' => 'Thumbnail width of the embedded media in px.',
  );

  $form['thumbnail_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Thumbnail height'),
    '#default_value' => variable_get('easy_embed_media_thumbnail_height', '165px'),
    '#description' => 'Thumbnail height of the embedded media.',
  );

  $form['player_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Player width'),
    '#default_value' => variable_get('easy_embed_media_player_width', '220px'),
    '#description' => 'Thumbnail width of the embedded media',
  );

  $form['player_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Player height'),
    '#default_value' => variable_get('easy_embed_media_player_height', '359px'),
    '#description' => 'Player height of the embedded media.',
  );
  
  $form['player_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Player width'),
    '#default_value' => variable_get('easy_embed_media_player_width', '100%'),
    '#description' => 'Player width of the embedded media.',
  );
  
  $form['#submit'][] = 'easy_embed_media_configure_form_submit';
  return system_settings_form($form);
}

/**
 * Form button submit callback.
 */
function easy_embed_media_configure_form_submit($form, &$form_state) {
  variable_set('easy_embed_media_thumbnail_width', $form_state['values']['thumbnail_width']);
  variable_set('easy_embed_media_thumbnail_height', $form_state['values']['thumbnail_height']);
  variable_set('easy_embed_media_player_height', $form_state['values']['player_height']);
  variable_set('easy_embed_media_player_width', $form_state['values']['player_width']);
}
// $Id: $
// Global killswitch: only run if we are in a supported browser.

if (Drupal.jsEnabled) {
  var ajaxmediaReq;
  Drupal.behaviors.easy_embed_media = function(context){
    
    easy_embed_media_process_preview(context);
    
    if (typeof CKEDITOR !== "undefined" && $("#edit-body").length > 0) {
      var ckeditorInstance = CKEDITOR.replace("edit-body");
      ckeditorInstance.on("instanceReady", function() {
        this.document.on("keyup", ckeditor_textchange);
        this.document.on("paste", ckeditor_textchange);
      });
      function ckeditor_textchange() {
        easy_embed_get_media_data(jQuery('cke_edit-body'), ckeditorInstance.getData(), 'cke_edit-body');
      }
    }
    jQuery('.form-textarea').bind('textchange', function() {
      easy_embed_get_media_data(jQuery(this), jQuery(this).val(), jQuery(this).attr('id'));
    });
    
  };
  
  function easy_embed_get_media_data(object, text, id) {
    for (key in Drupal.settings.easy_embed_media.platforms) {
      if (text.indexOf(Drupal.settings.easy_embed_media.platforms[key]) !== -1) {
        if (ajaxmediaReq != null) {
          ajaxmediaReq.abort();
        }
        if (jQuery('#' + id + '-easy-embed-media-preview').length == 0) {
          var div = document.createElement('div');
          jQuery(div).attr("id", id + '-easy-embed-media-preview');
          jQuery(div).addClass('easy-embed-media-embed-preview');
          object.after(div);
          jQuery(div).html('<div class="easy-embed-media-progress-bar"></div>');
          object.css("border-bottom", "none");
        } else {
          if (jQuery('.easy-embed-media-progress-bar').length == 0) {
            jQuery('#' + id + '-easy-embed-media-preview').append('<div class="easy-embed-media-progress-bar"></div>');
          }
        }
        
        ajaxmediaReq = jQuery.ajax({
          type: 'POST',
          url: window.location.href,
          data: '&popup=1&easy_embed_media_textarea_val=' + text.replace(/&/g, "%26") + '&easy_embed_media_textarea_id=' + id,
          dataType: 'json',
          success: easy_embed_media_ajax_success_event
        });
        break;
      }
    }
  }
  
  function easy_embed_media_ajax_success_event(result){
    if(result.result) {
      if (jQuery('#' + result.identifier + '-easy-embed-media-preview').length == 0) {
        var div = document.createElement('div');
        jQuery(div).attr("id", result.identifier + '-easy-embed-media-preview');
        jQuery(div).addClass('easy-embed-media-embed-preview');
        jQuery('#' + result.identifier).after(div);
        jQuery(div).html(result.result);
        $('#' + result.identifier).css("border-bottom", "none");
      } else {
        jQuery('#' + result.identifier + '-easy-embed-media-preview').html(result.result);
      }
      easy_embed_media_process_preview(jQuery('#' + result.identifier + '-easy-embed-media-preview'));
    }
  }
  
  function easy_embed_media_process_preview(context) {
    jQuery('.easy-embed-media-embed:not(.easy-embed-media-embed-processed)', context).addClass('easy-embed-media-embed-processed').each(function(){
      jQuery('.easy-embed-media-thumbnail',this).click(function() {
        jQuery('.easy-embed-media-player', jQuery(this).parent()).show();
        jQuery(this).hide();
      });
    });
  }
}

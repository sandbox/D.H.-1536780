<?php
// $Id$

/**
 * @file
 * Easy embed media theme functions.
 */

function theme_easy_embed_media_youtube($media) {
  $output = '<div class="easy-embed-media-embed clear-block">';
  $output .= '<div class="easy-embed-media-thumbnail">' . theme('image', drupal_get_path('module', 'easy_embed_media') . '/images/play.png', $media->title, $media->title, array('class' => 'easy-embed-media-thumbnail-play')) . theme('image', $media->thumbnail, $media->title, $media->title, array('width' => $media->thumbnail_width, 'height' => $media->thumbnail_height), FALSE) . '</div>';
  $output .= '<div class="easy-embed-media-player">' . $media->player . '</div>';
  $output .= '<div class="easy-embed-media-meta"><p>' . l($media->title, $media->url, array('absolute' => TRUE)) . '</p>';
  $output .= truncate_utf8($media->description, 250, TRUE, TRUE) . ' ' . l(t('more'), $media->url, array('absolute' => TRUE)) . '</div>';
  $output .= '</div>';
  return $output;
}

function theme_easy_embed_media_vimeo($media) {
  $output = '<div class="easy-embed-media-embed clear-block">';
  $output .= '<div class="easy-embed-media-thumbnail">' . theme('image', drupal_get_path('module', 'easy_embed_media') . '/images/play.png', $media->title, $media->title, array('class' => 'easy-embed-media-thumbnail-play')) . theme('image', $media->thumbnail, $media->title, $media->title, array('width' => $media->thumbnail_width, 'height' => $media->thumbnail_height), FALSE) . '</div>';
  $output .= '<div class="easy-embed-media-player">' . $media->player . '</div>';
  $output .= '<div class="easy-embed-media-meta"><p>' . l($media->title, $media->url, array('absolute' => TRUE)) . '</p>';
  $output .= truncate_utf8($media->description, 250, TRUE, TRUE) . ' ' . l(t('more'), $media->url, array('absolute' => TRUE)) . '</div>';
  $output .= '</div>';
  return $output;
}

function theme_easy_embed_media_soundcloud($media) {
  $output = '<div class="easy-embed-media-embed clear-block">';
  $output .= '<div class="easy-embed-media-thumbnail">' . theme('image', drupal_get_path('module', 'easy_embed_media') . '/images/play.png', $media->title, $media->title, array('class' => 'easy-embed-media-thumbnail-play')) . '<div class="easy-embed-media-thumbnail-empty" style="width:' . $media->thumbnail_width . '; height:' . $media->thumbnail_height . ';"></div></div>';
  $output .= '<div class="easy-embed-media-player">' . $media->player . '</div>';
  $output .= '<div class="easy-embed-media-meta"><p>' . l($media->title, $media->url, array('absolute' => TRUE)) . '</p>';
  $output .= truncate_utf8($media->description, 250, TRUE, TRUE) . ' ' . l(t('more'), $media->url, array('absolute' => TRUE)) . '</div>';
  $output .= '</div>';
  return $output;
}

function theme_easy_embed_media_mixcloud($media) {
  $output = '<div class="easy-embed-media-embed clear-block">';
  $output .= '<div class="easy-embed-media-thumbnail">' . theme('image', drupal_get_path('module', 'easy_embed_media') . '/images/play.png', $media->title, $media->title, array('class' => 'easy-embed-media-thumbnail-play')) . '<div class="easy-embed-media-thumbnail-empty" style="width:' . $media->thumbnail_width . '; height:' . $media->thumbnail_height . ';"></div></div>';
  $output .= '<div class="easy-embed-media-player">' . $media->player . '</div>';
  $output .= '<div class="easy-embed-media-meta"><p>' . l($media->title, $media->url, array('absolute' => TRUE)) . '</p>';
  $output .= truncate_utf8($media->description, 250, TRUE, TRUE) . ' ' . l(t('more'), $media->url, array('absolute' => TRUE)) . '</div>';
  $output .= '</div>';
  return $output;
}